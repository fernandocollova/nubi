import random
from tictaclib.exceptions import (
    InvalidColumnException,
    InvalidRowException,
    UsedCellException,
    PlayerWonException,
    MultipleMoveException,
)
from tictaclib import WINNING_BOARDS
from tictaclib import GAME_SIZE
from tictaclib.board import Board

class Player():

    size = GAME_SIZE

    def __init__(self, name="Player 1", symbol="X"):

        self.symbol = symbol
        self.name = name
        self.board = Board(self.size*[0, ])
        self.width = pow(2, self.size)
        self.valid_moves = [pow(2, limit) for limit in range(self.size)]

    @property
    def is_not_human(self):
        return False

    def restart(self):

        self.board = Board(self.size*[0, ])

    def chec_if_empty(self, row_num, col_num):

        row_data = self.get_row(row_num)
        return row_data ^ col_num >= row_data

    def get_row(self, row_num):

        try:
            return self.board[row_num]
        except IndexError:
            board_lenght = len(self.board)
            raise InvalidRowException(
                f"The row {row_num} is outside this {board_lenght} rows board"
            )

    def raise_for_winning_board(self):

        for board in WINNING_BOARDS:
            test_board = []
            for ref_row, row in zip(board, self.board):
                test_board.append(ref_row & row)
            if test_board in WINNING_BOARDS:
                raise PlayerWonException(f"{self.name} won the game")

    def place(self, row_num, move):

        row_data = self.get_row(row_num)

        if move >= self.width:
            raise InvalidColumnException(
                f"The move {move} is outside this {self.width} columns board"
            )

        if not move in self.valid_moves:
            raise MultipleMoveException("You can only fill one cell at a time")

        cell_is_empty = self.chec_if_empty(row_num, move)
        if cell_is_empty:
            self.board[row_num] = row_data | move
        else:
            raise UsedCellException(
                f"The user has alrady placed in {row_num},{move}"
            )

        self.raise_for_winning_board()

    def __str__(self):

        return self.name

    def __repr__(self):

        return self.__str__()


class IAPlayer(Player):

    def __init__(self, name="Player 2", symbol="O"):

        self.possible_rows = [number for number in range(self.size)]
        super().__init__(name, symbol)

    @property
    def is_not_human(self):
        return True
