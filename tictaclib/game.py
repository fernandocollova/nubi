from collections import deque
import random
from tictaclib.exceptions import (
    InvalidMoveException,
    InvalidColumnException,
    InvalidRowException,
    UsedCellException,
    BoardFilledException,
)
from tictaclib import GAME_SIZE
from tictaclib.board import Board

class Game():

    size = GAME_SIZE

    def __init__(self, players):

        self.turns = pow(2,self.size)

        random.shuffle(players)
        self.players = players
        self.current_player = 0

    def restart(self):

        for player in self.players:
            player.restart()
        random.shuffle(self.players)
        self.current_player = 0

    @property
    def current_player_object(self):
        
        return self.players[self.current_player]

    def raise_for_used_cell(self, row_num, col_num):

        for player in self.players:
            row_data = player.get_row(row_num)
            if row_data ^ col_num < row_data:
                raise UsedCellException(
                    f"Cell {row_num},{col_num} is used by {player}"
                )

    def raise_for_full_board(self):

        complete_board = Board(self.size*[0,])
        for player in self.players:
            complete_board = complete_board + player.board

        full_row = pow(2,self.size)-1
        if complete_board == self.size*[full_row,]:
            raise BoardFilledException()

    def next_player(self):

        self.current_player += 1
        if self.current_player >= len(self.players):
            self.current_player = 0

    def place(self, row_num, move):

        current_player = self.players[self.current_player]
        self.raise_for_used_cell(row_num, move)
        current_player.place(row_num, move)
        self.raise_for_full_board()

    def __repr__(self):

        return self.__str__()

    def __str__(self):

        formatter = f'{self.size}b'
        game_board = deque(self.size*[self.size*' ', ])

        for player in self.players:
            new_board = deque()
            for player_row in player.board:
                player_row = format(player_row, formatter)
                game_row = deque(game_board.popleft())
                new_row = []
                for player_cell in player_row:
                    game_cell = game_row.popleft()
                    if player_cell == '1':
                        new_row.append(player.symbol)
                    else:
                        new_row.append(game_cell)
                new_board.append(new_row)
            game_board = new_board

        return "\n".join(["|".join(row) for row in game_board])

    def __iter__(self):
        return self

    def __next__(self):
        self.raise_for_full_board()
        current_player = self.players[self.current_player]
        self.next_player()
        return current_player


class IAGame(Game):

    def __init__(self, players):
        super().__init__(players)
        if self.current_player_object.is_not_human:
            self.auto_place()
            self.next_player()

    def auto_place(self):

        current_player = self.players[self.current_player]
        move = None
        while not move:
            try:
                maybe_row = random.choice(current_player.possible_rows)
                maybe_move = random.choice(current_player.possible_rows)
                maybe_move = pow(2, maybe_move)
                self.raise_for_used_cell(maybe_row, maybe_move)
                move = maybe_row, maybe_move
            except InvalidMoveException:
                pass
        self.place(*move)

    def restart(self):

        super().restart()
        current_player = self.players[self.current_player]
        if current_player.is_not_human:
            self.auto_place()
            self.next_player()

    def next_player(self):

        super().next_player()
        current_player = self.players[self.current_player]
        if current_player.is_not_human:
            self.auto_place()
            self.next_player()
