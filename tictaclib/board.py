class Board(list):

    def __add__(self, other):

        if len(other) != len(self):
            raise ValueError("You can't add boards of different sizes")
        
        return Board(
            [own_row | other_row for own_row, other_row in zip(self, other)]
        )
