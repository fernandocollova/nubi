from argparse import ArgumentParser

class TicTacArgParser(ArgumentParser):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.add_argument(
            '-i', '--no-ia',
            help="Play between two humans.",
            action="store_false",
            dest='ia',
            default=True,
        )
