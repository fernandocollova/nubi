WINNING_BOARDS = [
    [7,0,0],[0,7,0],[0,0,7], # Horizontal
    [4,4,4],[2,2,2],[1,1,1], # Vertical
    [4,2,1],[1,2,4] # Diagonals
]

GAME_SIZE = 3
