class PlayerWonException(Exception):
    pass

class BoardFilledException(StopIteration):
    pass

class InvalidMoveException(Exception):
    pass

class InvalidColumnException(InvalidMoveException):
    pass

class InvalidRowException(InvalidMoveException):
    pass

class UsedCellException(InvalidMoveException):
    pass

class MultipleMoveException(InvalidMoveException):
    pass
