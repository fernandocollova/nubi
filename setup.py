from setuptools import setup

setup(
    name='tictactoe',
    version="0.0.3",
    description='Tic Tac Toe game',
    packages=[
        'tictaclib',
    ],
    scripts=[
        'tictactoe.py',
    ],
    install_requires=[
        "urwid",
    ]
)
