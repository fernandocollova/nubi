import unittest
import copy
from tictaclib.player import Player
from tictaclib.game import Game
from tictaclib.exceptions import (
    PlayerWonException,
    UsedCellException,
    InvalidColumnException,
    InvalidRowException,
    MultipleMoveException,
    BoardFilledException,
)


class TestPlacingConflicts(unittest.TestCase):

    def test_placing_in_own_used_place_fails(self):
        player = Player()
        player.place(1, 1)
        self.assertRaises(UsedCellException, player.place, 1, 1)

    def test_placing_outside_board_columns_fails(self):
        player = Player()
        self.assertRaises(InvalidColumnException, player.place, 2, 8)

    def test_placing_outside_board_rows_fails(self):
        player = Player()
        self.assertRaises(InvalidRowException, player.place, 4, 2)


class TestPlayerMovement(unittest.TestCase):

    def test_player_wins_after_winning_moves(self):
        player = Player()
        player.place(0, 4)
        player.place(1, 4)
        self.assertRaises(PlayerWonException, player.place, 2, 4)

    def test_player_cannot_fill_two_cells_at_once(self):
        player = Player()
        self.assertRaises(MultipleMoveException, player.place, 2, 3)

    def test_player_moves_correctly(self):
        player = Player()
        player.place(0, 1)
        self.assertEqual(player.board, [1, 0, 0])


class TestGame(unittest.TestCase):

    tie_moves = [
        [(0, 1), (1, 4), (2, 1), (1, 2)],
        [(2, 4), (1, 1), (0, 2), (2, 2), (0, 4)],
    ]
    tie_board_0 = [6, 1, 6]
    tie_board_1 = [1, 6, 1]

    def test_placing_in_other_used_place_fails(self):
        game = Game(players=[])
        game.players = [
            Player(name="Player 1", symbol="X"),
            Player(name="Player 2", symbol="O"),
        ]
        game.place(1, 1)
        game.current_player = 1
        self.assertRaises(UsedCellException, game.place, 1, 1)

    def test_game_ends_on_full_board(self):
        game = Game(players=[])
        game.players = [
            Player(name="Player 1", symbol="X"),
            Player(name="Player 2", symbol="O"),
        ]
        game.players[0].board = self.tie_board_0
        game.players[1].board = self.tie_board_1
        self.assertRaises(BoardFilledException, game.raise_for_full_board)

    def iter_game(self, game, moves):
        for player in game:
            move = moves[game.current_player].pop()
            player.place(*move)

    def test_players_iteration(self):
        game = Game(players=[])
        game.players = [
            Player(name="Player 1", symbol="X"),
            Player(name="Player 2", symbol="O"),
        ]
        self.iter_game(game, copy.deepcopy(self.tie_moves))
        self.assertEqual(game.players[0].board, self.tie_board_0)
        self.assertEqual(game.players[1].board, self.tie_board_1)

    def test_players_manual_iteration(self):
        game = Game(players=[])
        game.players = [
            Player(name="Player 1", symbol="X"),
            Player(name="Player 2", symbol="O"),
        ]
        game.current_player = 1
        tie_moves = copy.deepcopy(self.tie_moves)
        for _ in range(8):
            move = tie_moves[game.current_player].pop()
            game.place(*move)
            game.next_player()
        last_move = tie_moves[game.current_player].pop()
        self.assertRaises(BoardFilledException, game.place, *last_move)
