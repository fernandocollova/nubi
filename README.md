# Tic Tac Toe for your terminal

![Tic Tac Toe](https://1.bp.blogspot.com/-A-oMUjtbvAk/WpDXW_0QGdI/AAAAAAAAG6Y/5Dxc4pFiZCMZ2uft5ciXHEvzO39MDNM3wCLcBGAs/s1600/ico.png)


## How to use it?

First of all, you need to install it with `pip3 install --user git+https://fernandocollova@bitbucket.org/fernandocollova/nubi.git`

Then, if you want play against the super ultra advanced built in IA, simply run `ticatactoe.py` from the terminal

Or, if you want to play agains another human being, run `ticatactoe.py -i` to disable the IA player 

The board is presented in the following layout

```
  c b a
1
2
3
```

And you move by entering the coordinates of the the place you want to fill. For example, to place in the center of the board, you can input `2b` to get the following board:

```
  c b a
1
2   X
3
```
