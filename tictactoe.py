import urwid
import re
from string import ascii_lowercase
from tictaclib.game import Game, IAGame
from tictaclib.player import Player, IAPlayer
from tictaclib.argparse import TicTacArgParser
from tictaclib.exceptions import (
    InvalidMoveException,
    PlayerWonException,
    BoardFilledException,
)


def create_default_game(ia):

    players = [
        Player(name="Player 1", symbol="X"),
    ]

    if ia:
        players.append(IAPlayer(name="Player 2", symbol="O"))
        return IAGame(players)
    else:
        players.append(Player(name="Player 2", symbol="O"))
        return Game(players)


class QuestionBox(urwid.Filler):

    def __init__(self, game, *args, **kwargs):

        self.game = game

        valid_rows = self.game.size
        valid_columns = ascii_lowercase[:valid_rows]
        self.valid_move = re.compile(f'[0-{valid_rows}][{valid_columns}]')

        initial_state = urwid.Edit(str(self.game) + '\n' + self.question)
        super().__init__(initial_state, *args, **kwargs)

    @property
    def question(self):
        return f"{self.game.current_player_object.name}, Your move?: "

    def restart(self):
        self.game.restart()
        return urwid.Edit(str(self.game) + '\n' + self.question)

    def input_is_valid(self, user_input):

        if user_input in ("restart", "quit"):
            return True
        elif self.valid_move.fullmatch(user_input):
            return True
        else:
            return False

    def parse_move(self, text):

        move_parts = list(text)
        row = int(move_parts[0])-1
        index = ascii_lowercase.index(move_parts[1]) + 1
        move = pow(2, self.game.size - index)
        return row, move

    def keypress(self, size, key):

        if key != 'enter':
            return super(QuestionBox, self).keypress(size, key)

        text_input = self.original_widget.edit_text
        if not self.input_is_valid(text_input):
            message = "Please provide a valid input: "
            self.original_widget = urwid.Edit(str(self.game) + "\n" + message)
            return

        if text_input == "restart":
            self.original_widget = self.restart()
            return
        elif text_input == "quit":
            raise urwid.ExitMainLoop()
        else:
            row, move = self.parse_move(text_input)

        try:
            self.game.place(row, move)
            self.game.next_player()
            board_state = urwid.Edit(str(self.game) + "\n" + self.question)
        except InvalidMoveException as e:
            message = "\n" + str(e) + "\nPlease try again: "
            board_state = urwid.Edit(str(self.game) + message)
            del e
        except PlayerWonException as e:
            message = str(self.game) + "\n" + str(e)
            message += "\n" + 'Type "restart" to play again or "quit" to quit: '
            board_state = urwid.Edit(message)
            del e
        except BoardFilledException:
            message = str(self.game) + "\n" + "Nobody won"
            message += "\n" + 'Type "restart" to play again or "quit" to quit: '
            board_state = urwid.Edit(message)

        self.original_widget = board_state


def main(ia):

    fill = QuestionBox(create_default_game(ia))
    loop = urwid.MainLoop(fill)
    loop.run()


if __name__ == "__main__":
    try:
        args_parser = TicTacArgParser()
        args = args_parser.parse_args()
        main(**vars(args))
    except KeyboardInterrupt:
        pass
